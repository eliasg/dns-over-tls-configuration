# DNS over TLS configuration

Configure DNS over TLS on Linux

## Installation

*  Debian/Ubuntu etc 
```
$ sudo aptitude install stubby
$ sudo systemctl enable stubby
```
* Fedora/Centos

```
$ sudo dnf install getdns
$ sudo systemctl enable stubby
```

Stubby listens on TCP and UDP port 53 of localhost (127.0.0.1), as can be seen by running this command:

```
$ sudo netstat -lnptu | grep stubby
```

The main configuration file is `/etc/stubby/stubby.yml`. 

```
$ sudo vi /etc/stubby/stubby.yml
```

Keep the following settings:

```
resolution_type: GETDNS_RESOLUTION_STUB

dns_transport_list:
- GETDNS_TRANSPORT_TLS

tls_authentication: GETDNS_AUTHENTICATION_REQUIRED

listen_addresses:
- 127.0.0.1
- 0::1

round_robin_upstreams: 1
```

### Configure NetworkManager

IPv4 settings:
Method: Change to `Automatic(DHCP) addresses only` 

DNS server to `127.0.0.1` 

IPv6 settings:

Method: Change to `Automatic, addresses only` 

DNS servers to  `::1`

### Only Use Cloudflare DNS

Edit code to 

```
  - address_data: 1.1.1.1
    tls_auth_name: "cloudflare-dns.com"
  - address_data: 1.0.0.1
    tls_auth_name: "cloudflare-dns.com"
```

and 

```
round_robin_upstreams: 0
```

After that 
```
$ sudo systemctl restart stubby
```
## References

* https://www.linuxbabe.com/ubuntu/ubuntu-stubby-dns-over-tls